plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.71"

    jacoco

    id("org.sonarqube") version "2.8"

    // Apply the application plugin to add support for building a CLI application.
    application
}

dependencies {
    "implementation"(project(":application"))
    "implementation"(project(":ports-infra"))
}

allprojects {
    apply(plugin = "java")
    apply(plugin = "jacoco")

    repositories {
        jcenter()
    }

    group = "be.duponter.swccg"
    version = "0.1.0"

    dependencies {
        val arrowVersion = "0.10.5"

        val junit5Version = "5.6.2"
        val assertKVersion = "0.22"

        // Align versions of all Kotlin components
        "implementation"(platform("org.jetbrains.kotlin:kotlin-bom"))

        // Use the Kotlin JDK 8 standard library.
        "implementation"("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        "implementation"("io.arrow-kt:arrow-core:${arrowVersion}")

        // Use the Kotlin test library.
        "testImplementation"("org.jetbrains.kotlin:kotlin-test")

        // Use the Kotlin JUnit 5 integration.
        "testImplementation"("org.jetbrains.kotlin:kotlin-test-junit5")
        "testImplementation"("org.junit.jupiter:junit-jupiter-api:${junit5Version}")
        "testImplementation"("com.willowtreeapps.assertk:assertk:${assertKVersion}")

        "testRuntimeOnly"("org.junit.jupiter:junit-jupiter-engine:${junit5Version}")
    }

    tasks.named<Test>("test") {
        useJUnitPlatform()
        finalizedBy("jacocoTestReport")
    }

    jacoco {
        toolVersion = "0.8.5"

        tasks.jacocoTestReport {
            reports {
                xml.isEnabled = true
                csv.isEnabled = false
                html.isEnabled = false
            }
        }
    }

    tasks.jacocoTestCoverageVerification {
        violationRules {
            rule {
                limit {
                    minimum = "0.8".toBigDecimal()
                }
            }
        }
    }
}

application {
    // Define the main class for the application.
    mainClassName = "be.duponter.swccg.AppKt"
}
