package be.duponter.swccg.application

import arrow.core.Either
import be.duponter.swccg.ports.infra.PortsInfra

class Application {
    fun oneOf(): Either<String, String> {
        return Either.right(forwardingDependencyMethod())
    }

    fun someLibraryMethod(): String {
        return "Print out from application module"
    }

    fun forwardingDependencyMethod(): String {
        return "Forwarding: ${PortsInfra().someLibraryMethod()}"
    }
}
