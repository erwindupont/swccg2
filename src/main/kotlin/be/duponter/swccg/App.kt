package be.duponter.swccg

import be.duponter.swccg.application.Application
import be.duponter.swccg.ports.infra.PortsInfra

class App {
    val greeting: String
        get() {
            return "Print out from root project"
        }
}

fun main(args: Array<String>) {
    println(App().greeting)
    println(Application().someLibraryMethod())
    println(Application().forwardingDependencyMethod())
    println(PortsInfra().someLibraryMethod())
}
