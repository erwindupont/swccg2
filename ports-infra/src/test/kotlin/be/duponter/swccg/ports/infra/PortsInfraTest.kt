package be.duponter.swccg.ports.infra

import kotlin.test.Test
import kotlin.test.assertEquals

class PortsInfraTest {
    @Test
    fun testSomeLibraryMethod() {
        val classUnderTest = PortsInfra()
        assertEquals("Print out from ports infra module", classUnderTest.someLibraryMethod())
    }
}
